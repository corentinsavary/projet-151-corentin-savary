# Projet 151 - Corentin Savary

# Cara Dog Attitude


# Objectifs : 

Segments utilisateurs : Ceux qui ont besoin d’éduquer leur chien
Problème de besoin : ceux qui n’arrivent pas à éduquer leur chien et qui ont besoin d’aide 
Proposition de valeur : Faire un calendrier ou les gens peuvent prendre rendez vous pour venir 
Critère de succès :  Gagner de l’argent en éduquant des chiens

# Prototype : 

C’est une application web afin de proposer les services d’éducation pour les chiens.
Il y aura la possibilité de réserver avec agenda.ch par exemples
Modifier les données avec le compte admin

# MVP : 

Calendrier agenda.ch
Possibilité de modification du texte avec compte admin

# POC : 

Page administration avec possibilité modifié les prestations
Clique ouvrir sur étude du comportement et ça redirige sur le formulaire avec le texte explicatif et dire que c’est l’étude de comportement avec la réservation.


# Pages : 

Accueil (accueil.php)
Réservation (reservation.php)
Administration(admin.php)


# Communications:

|Verbe HTTP  |   Endpoint      |  Données                        | Description                |
|------------|-----------------|---------------------------------|----------------------------|
|GET         |   Accueil.php   |                                 |  Affiche la page           |
|GET         | Reservation.php |                                 |  Affiche la page           |
|GET         |   Admin.php     |                                 |  Affiche la page           |
|PUT         |   Accueil.php   |  text, url_img, id_deroulement  |   Modifie le texte         |
|POST        |   Accueil.php   |  text, url_img                  |   Créer le texte           |
|DELETE      |   Accueil.php   |  texte, url_img, id_deroulement |   Delete le texte          |


# Base de données

|Chien      |               
---------   |
|id_chien   |
|nom_chien  |
|age_chien  |
|id_client  |


|Client        |
---------      |
|id_client     |
|nom_client    |
|prenom_client |
|tel_client    |
|email_client  |


|Text             |
---------         |
|id_deroulement   |
|texte            |
|url_img          |



